import {MOCK_LAPTOPS} from "./computers.mock.js";

const elBalance = document.getElementById("balance")
const elWorkBalance = document.getElementById("work-balance")
const elLoanAmount = document.getElementById("loan-amount")
const elLaptopInfo = document.getElementById("laptop-info")
const elLaptopName = document.getElementById("laptop-name")
const elLaptopDescription = document.getElementById("laptop-description")
const elLaptopImg = document.getElementById("laptop-img")
const elLaptopFeatures = document.getElementById("feature-list")
const elLaptopPrice = document.getElementById("laptop-price")
const elPayLoanButton = document.getElementById("pay-loan")
const elLaptopDropMenu = document.getElementById("laptop")

function StoreHandler() {
    this.bankMoney = 0
    this.loanSize = 0
    this.pocketMoney = 0
    this.hasBoughtPcSinceLastLoan = true
    this.selectedLaptopId = -1

    this.sendPocketMoneyToBank = function() {
        if(this.loanSize > 0) {
            // If there is a loan, first subtract 10% from pocket money and the loan
            let payOffLoan = this.pocketMoney * 0.1
            this.loanSize -= payOffLoan
            this.pocketMoney -= payOffLoan
            // If the loan has gone below zero, put the difference back and set the loan to zero
            if(this.loanSize < 0) {
                this.pocketMoney -= this.loanSize
                this.loanSize = 0
            }
        }
        this.bankMoney += this.pocketMoney
        this.pocketMoney = 0
        this.render()
    }

    this.doWork = function() {
        this.pocketMoney += 100
        this.render()
    }

    this.loanMoney = function() {
        // Ask user for loan size
        const userInput = prompt('Please enter how much you want to loan: ')

        // Make sure it is a valid input
        if(isNaN(userInput) || userInput === "" || Number(userInput) === 0) {
            window.alert('Please enter a valid number.')
            return
        }

        const amount = Number(userInput)
        // Check that the user is allowed to take a loan and provide a correctional message if they are not.
        if(amount > this.bankMoney * 2) {
            window.alert('You do not have enough money to loan that much.')
            return
        }
        if(this.loanSize > 0) {
            window.alert('You need to pay off your loan first!')
            return
        }
        if(!this.hasBoughtPcSinceLastLoan) {
            window.alert('You need to buy a computer before taking another loan.')
            return
        }

        // Update values and render them
        this.bankMoney += amount
        this.loanSize = amount
        this.hasBoughtPcSinceLastLoan = false
        this.render()
    }

    // Let the user pay off their loan using pocket money
    this.payLoan = function() {
        if(this.loanSize < this.pocketMoney) {
            this.pocketMoney -= this.loanSize
            this.loanSize = 0
        } else {
            this.loanSize -= this.pocketMoney
            this.pocketMoney = 0
        }
        this.render()
    }

    this.changeSelectedLaptop = function() {
        this.selectedLaptopId = Number(elLaptopDropMenu.value)

        // Remove old items from feature list
        while(elLaptopFeatures.hasChildNodes()) {
            elLaptopFeatures.removeChild(elLaptopFeatures.firstChild)
        }
        // Remove display if none has been selected
        if(this.selectedLaptopId === -1) {
            elLaptopInfo.style.display = 'none'
            return
        }
        // Otherwise, make it visible
        elLaptopInfo.style.display = 'block'
        // Get selected laptop
        const selectedLaptop = MOCK_LAPTOPS.find(laptop => laptop.id === this.selectedLaptopId )
        // Display name of chosen laptop and the description
        elLaptopName.innerText = selectedLaptop.name
        elLaptopDescription.innerText = selectedLaptop.description

        // Display price of the laptop
        elLaptopPrice.innerText = `Price: ${selectedLaptop.price} Kr.`
        // Display image
        elLaptopImg.src = selectedLaptop.imgSrc
        // Add the current laptop's features to feature list
        for(const description of selectedLaptop.features) {
            const itemInList = document.createElement("li")
            itemInList.appendChild(document.createTextNode(description))
            elLaptopFeatures.appendChild(itemInList)
        }
    }

    // Buy the currently selected laptop
    this.buyLaptop = function () {
        if(this.selectedLaptopId === -1) {
            return
        }
        if(MOCK_LAPTOPS[this.selectedLaptopId].price > this.bankMoney) {
            window.alert('You do not have enough money in the bank to buy this laptop!')
        } else {
            this.bankMoney -= MOCK_LAPTOPS[this.selectedLaptopId].price
            this.hasBoughtPcSinceLastLoan = true
            window.alert('You have bought: ' + MOCK_LAPTOPS[this.selectedLaptopId].name)
            this.render()
        }
    }

    // Render values from the StoreHandler
    this.render = function() {
        elWorkBalance.innerText = 'Work balance: ' + this.pocketMoney + ' NOK.'
        elBalance.innerText = 'Balance: ' + this.bankMoney + ' NOK.'
        if(this.loanSize > 0){
            elLoanAmount.style.display = 'block'
            elLoanAmount.innerText = 'Loan: ' + this.loanSize + ' NOK.'
            elPayLoanButton.style.display = ''
        } else {
            elLoanAmount.style.display = 'none'
            elPayLoanButton.style.display = 'none'
        }
    }
}

export default StoreHandler