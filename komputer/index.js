import {MOCK_LAPTOPS} from "./computers.mock.js";
import StoreHandler from "./storehandler.js";

const elGetLoan = document.getElementById("get-loan")
const elDoWork = document.getElementById("do-work")
const elBankMoney = document.getElementById("bank-money")
const elPayLoanButton = document.getElementById("pay-loan")
const elLaptopDropMenu = document.getElementById("laptop")
const elBuyButton = document.getElementById("buy-laptop")

// Add all the laptops to the drop-down menu
for(const laptop of MOCK_LAPTOPS) {
    const option = document.createElement("option")
    option.text = laptop.name
    option.value = laptop.id
    elLaptopDropMenu.add(option)
}

const storeHandler = new StoreHandler()
storeHandler.render()

// Event handler for starting a loan
elGetLoan.addEventListener('click', function(){
    storeHandler.loanMoney()
})

// Event handler for the work button
elDoWork.addEventListener("click", function(){
    storeHandler.doWork()
})

// Event handler for sending money from the pocket wallet to the bank
elBankMoney.addEventListener("click", function() {
    storeHandler.sendPocketMoneyToBank()
})

// Event handler for paying a loan
elPayLoanButton.addEventListener("click", function() {
    storeHandler.payLoan()
})

// Display info about a laptop when the user selects it
elLaptopDropMenu.addEventListener("change", function(){
    storeHandler.changeSelectedLaptop()
})

// Event handler for buying a laptop
elBuyButton.addEventListener("click", function(){
    storeHandler.buyLaptop()
})