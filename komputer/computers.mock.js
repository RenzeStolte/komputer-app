// Create array with different laptops
export const MOCK_LAPTOPS =
    [
        {
            id: 0,
            name: "ASUZ Zenbooc",
            imgSrc: "https://www.intel.com/content/dam/products/catalog/global/images/1x1/devices-laptops-1x1.png.rendition.intel.web.550.550.png",
            description: "Some people would recommend this laptop.",
            features: [
                "Best processing power!",
                "Fan power that will blow you away!",
                "Guaranteed broken in two years!"
            ],
            price: 1000
        },
        {
            id: 1,
            name: "Aser Aspore 5",
            description: "Aser offers the best in some technology, presumably",
            imgSrc: "https://www.elkjop.no/image/dv_web_D180001002430099/161508/acer-aspire-3-156-baerbar-pc-sort.jpg?$prod_all4one$",
            features: [
                "Good quality screen",
                "Very good",
                "Very nice",
                "Excellent"
            ],
            price: 2000
        },
        {
            id: 2,
            name: "Teslo Madel Es",
            description: "A laptop that doubles as a car!",
            imgSrc: "https://smartcdn.prod.postmedia.digital/driving/wp-content/uploads/2019/04/model-s-001.jpg?quality=100&strip=all",
            features: [
                "Decorative harness around the laptop!",
                "Easy to bring anywhere thanks to automated move system!",
                "Might not fit in your bag."
            ],
            price: 1008712
        },
        {
            id: 3,
            name: "HP ProBooc",
            imgSrc: "https://www.elkjop.no/image/dv_web_D180001002700901/254491/hp-probook-450-g8-156-barbar-pc-solv.jpg?$prod_all4one$",
            description: "Laptop inspired by the famous Harry Potter series!",
            features: [
                "9 GB's of RAM.",
                "15,60 inches screen with full HD.",
                "Wi-Fi 6 (802.11ax) WIFI generation.",
                "5 FR of Jibbels.",
                "Litium-ion batteries."
            ],
            price: 3000
        }
    ]
