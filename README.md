# JavaScript Assignment 1 - Komputer App

#### by Renze Stolte

## Description
Komputer App is a simple webpage that lets the user get money and spend it on a laptop of choice.

### Work
In order to get money, the user has to press on the "Do work" button. This will give them 100 kroners in their "pocket" wallet. This money can then either be stored in the bank, or used to pay off a loan.

### Loan
The user can take a loan. Some rules apply: The user has to buy a laptop after taking a loan before they can buy a new one. The user cannot take a loan if they still owe money. Finally, the user cannot get a loan greater than twice their Bank balance.
Whenever the user transgers money from their pocket wallet to the bank, 10% will be used to pay off their loan.

### Buy laptop
The user can select a laptop in a drop-down menu. Information about the laptop will then be displayed. If the user presses buy and has enough money in their bank, they will buy the laptop, and the cost will be subtracted from their bank balance.
